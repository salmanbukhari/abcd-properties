package com.abcdproperties.abcd.util;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class Util {

    private Util(){

    }

    /**
     * Hash a password using the OpenBSD bcrypt scheme
     *
     * @param sequence plain String sequence
     *
     * @return return hashed password
     */
    public static String bCryptSequence (final String sequence) {
        return (BCrypt.hashpw(sequence, BCrypt.gensalt(12)));
    }

    /**
     * This method verifies if given plain String sequence matches the given hash
     *
     * @param sequence plain String sequence
     * @param hash     generated hash for the sequence
     *
     * @return true if matches otherwise false
     */
    public static boolean bCryptMatch (final String sequence, final String hash) {
        return (BCrypt.checkpw(sequence, hash));
    }

}

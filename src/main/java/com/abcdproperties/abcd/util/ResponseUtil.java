package com.abcdproperties.abcd.util;

import com.abcdproperties.abcd.dto.ResponseObject;

public class ResponseUtil {
    private ResponseUtil() {

    }

    /**
     * @param status
     * @param message
     * @param object
     *
     * @return
     */
    public static ResponseObject createResponseObject (String status, String message, Object object) {
        ResponseObject responseObject = new ResponseObject();
        responseObject.setMessage(message);
        responseObject.setStatus(status);
        responseObject.setObject(object);
        return responseObject;
    }
}

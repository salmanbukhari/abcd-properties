package com.abcdproperties.abcd.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

    private DateUtil(){

    }

    public static Calendar getFormatedDate(String dateString){
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    /**
     * This function is supposed to convert Calendar object to String
     *
     * @param calendar
     *
     * @return String
     */
    public static String convertCalendarToString (Calendar calendar) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return (simpleDateFormat.format(calendar.getTime()));
    }

}

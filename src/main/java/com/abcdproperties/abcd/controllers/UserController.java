package com.abcdproperties.abcd.controllers;

import com.abcdproperties.abcd.dto.ResponseObject;
import com.abcdproperties.abcd.dto.UserDTO;
import com.abcdproperties.abcd.services.UserDetailService;
import com.abcdproperties.abcd.util.AppConstants;
import com.abcdproperties.abcd.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {

    @Autowired
    private UserDetailService userDetailService;

    @GetMapping(value = "/checkEmailExists")
    public ResponseEntity<ResponseObject> checkEmailExists(@RequestParam String email){
        return ResponseEntity.ok(ResponseUtil.createResponseObject(AppConstants.SUCCESS, null, userDetailService.checkEmailExist(email)));
    }

    @PostMapping(value = "/signup")
    public ResponseEntity<ResponseObject> signUpUser(@RequestBody UserDTO userRequest){
        return ResponseEntity.ok(ResponseUtil.createResponseObject(AppConstants.SUCCESS, userDetailService.saveUser(userRequest), userRequest));
    }
}

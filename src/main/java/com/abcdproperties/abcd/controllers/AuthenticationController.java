package com.abcdproperties.abcd.controllers;

import com.abcdproperties.abcd.dto.RequestLogin;
import com.abcdproperties.abcd.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class AuthenticationController {

    @Autowired
    private AuthService authService;


    /**
     * This service endpoint will be used to authenticate user for first time.
     * This service will return auth token and that token will be passed in every request.
     * If token is not passed then request will not be served.
     * @param requestLogin Request data containing username and password
     *
     * @return ResponseLogin object containing auth token
     *
     */
    @PostMapping(value = "/auth")
    public ResponseEntity<Object> authenticationRequest(@Valid @RequestBody RequestLogin requestLogin)  {
        return ResponseEntity.ok(authService.processAuthentication(requestLogin));
    }
}

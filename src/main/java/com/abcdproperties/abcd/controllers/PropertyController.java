package com.abcdproperties.abcd.controllers;


import com.abcdproperties.abcd.dto.PropertyDTO;
import com.abcdproperties.abcd.dto.PropertyFilterDTO;
import com.abcdproperties.abcd.dto.ResponseObject;
import com.abcdproperties.abcd.services.PropertyServiceImpl;
import com.abcdproperties.abcd.util.AppConstants;
import com.abcdproperties.abcd.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PropertyController {

    @Autowired
    private PropertyServiceImpl propertyService;

    @PostMapping(value = "/getPropertyByFilter")
    private ResponseObject getAllProperties (@RequestBody PropertyFilterDTO propertyFilterDTO) {
        return ResponseUtil.createResponseObject(AppConstants.SUCCESS, "", propertyService.getPropertyByFilter(propertyFilterDTO));
    }

    @PostMapping (value = "/saveUpdateProperty")
    private ResponseObject saveUpdateProperty (@RequestBody PropertyDTO propertyDTO) {
        return ResponseUtil.createResponseObject(AppConstants.SUCCESS, propertyService.saveUpdateProperty(propertyDTO), null);
    }

    @PostMapping(value = "/deleteProperty")
    private ResponseObject deleteProperty (@RequestBody PropertyDTO propertyDTO) {
        return ResponseUtil.createResponseObject(AppConstants.SUCCESS, propertyService.saveUpdateProperty(propertyDTO), null);
    }
}

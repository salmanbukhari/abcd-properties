package com.abcdproperties.abcd.controllers;


import com.abcdproperties.abcd.dto.ResponseObject;
import com.abcdproperties.abcd.services.CityService;
import com.abcdproperties.abcd.util.AppConstants;
import com.abcdproperties.abcd.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CityController {

    @Autowired
    private CityService cityService;

    @GetMapping(value = "/cities")
    private ResponseObject getCities () {
        return ResponseUtil.createResponseObject(AppConstants.SUCCESS, "", cityService.getCitiesList());
    }
}

package com.abcdproperties.abcd.controllers;


import com.abcdproperties.abcd.dto.ResponseObject;
import com.abcdproperties.abcd.services.PropertyTypeService;
import com.abcdproperties.abcd.util.AppConstants;
import com.abcdproperties.abcd.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PropertyTypeController {

    @Autowired
    private PropertyTypeService propertyTypeService;

    @GetMapping(value = "/propertyType")
    private ResponseObject getPropertyType () {
        return ResponseUtil.createResponseObject(AppConstants.SUCCESS, "",propertyTypeService.getPropertyTypes());
    }
}

package com.abcdproperties.abcd.controllers;


import com.abcdproperties.abcd.dto.ResponseObject;
import com.abcdproperties.abcd.services.CountrService;
import com.abcdproperties.abcd.util.AppConstants;
import com.abcdproperties.abcd.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountryController {

    @Autowired
    private CountrService countrService;

    @GetMapping(value = "/countries")
    private ResponseObject getCountries () {
        return ResponseUtil.createResponseObject(AppConstants.SUCCESS, "", countrService.getCountriesList());
    }
}

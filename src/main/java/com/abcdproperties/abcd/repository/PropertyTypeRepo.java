package com.abcdproperties.abcd.repository;

import com.abcdproperties.abcd.model.PropertyType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyTypeRepo extends JpaRepository <PropertyType, Integer> {

    PropertyType getByName(String name);
}

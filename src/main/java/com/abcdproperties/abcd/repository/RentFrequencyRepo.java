package com.abcdproperties.abcd.repository;

import com.abcdproperties.abcd.model.RentFrequency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RentFrequencyRepo extends JpaRepository<RentFrequency, Integer> {

    RentFrequency getByName(String name);

    RentFrequency getByCode(String code);
}

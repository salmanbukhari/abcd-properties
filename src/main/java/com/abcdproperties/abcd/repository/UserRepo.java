package com.abcdproperties.abcd.repository;

import com.abcdproperties.abcd.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Integer> {

    User findByEmail(String userName);
}

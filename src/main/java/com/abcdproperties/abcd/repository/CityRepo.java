package com.abcdproperties.abcd.repository;

import com.abcdproperties.abcd.model.City;
import com.abcdproperties.abcd.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepo extends JpaRepository<City, Integer> {

    City getByName (String name);

    City getByCode (String code);

    City getCitiesByCountry (Country country);
}

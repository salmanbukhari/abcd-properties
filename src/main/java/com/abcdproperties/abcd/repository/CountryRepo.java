package com.abcdproperties.abcd.repository;

import com.abcdproperties.abcd.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepo extends JpaRepository<Country, Integer> {

    Country getByName (String name);

    Country getByCode (String code);

}

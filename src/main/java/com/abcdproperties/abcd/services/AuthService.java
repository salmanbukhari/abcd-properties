package com.abcdproperties.abcd.services;

import com.abcdproperties.abcd.dto.RequestLogin;
import com.abcdproperties.abcd.dto.ResponseLogin;
import com.abcdproperties.abcd.security.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailService userDetailsService;

    @Autowired
    private TokenUtils tokenUtils;

    public ResponseLogin processAuthentication(RequestLogin requestLogin) {
        try {
            Authentication authentication = this.authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(requestLogin.getEmail(), requestLogin.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (Exception e) {
            throw new BadCredentialsException("");
        }
        // Reload password post-authentication so we can generate token
        UserDetails userDetails;
        try {
            userDetails = this.userDetailsService.loadUserByEmail(requestLogin.getEmail());
        } catch (UsernameNotFoundException e) {
            throw new BadCredentialsException("");
        }
        ResponseLogin responseLogin = new ResponseLogin();
        String token = this.tokenUtils.generateToken(userDetails);
        responseLogin.setToken(token);
        return responseLogin;
    }
}

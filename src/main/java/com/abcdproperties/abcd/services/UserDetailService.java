package com.abcdproperties.abcd.services;

import com.abcdproperties.abcd.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserDetailService extends UserDetailsService {

    UserDetails loadUserByEmail(String var1) throws UsernameNotFoundException;

    boolean checkEmailExist (String email);

    String saveUser(UserDTO userRequest);
}

package com.abcdproperties.abcd.services;

import com.abcdproperties.abcd.dto.PropertyDTO;
import com.abcdproperties.abcd.dto.PropertyFilterDTO;
import com.abcdproperties.abcd.exception.PropertyException;
import com.abcdproperties.abcd.model.*;
import com.abcdproperties.abcd.repository.*;
import com.abcdproperties.abcd.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class PropertyServiceImpl {

    @Autowired
    protected PropertyRepo propertyRepo;

    @Autowired
    private PropertyTypeRepo propertyTypeRepo;

    @Autowired
    private RentFrequencyRepo rentFrequencyRepo;

    @Autowired
    private CountryRepo countryRepo;

    @Autowired
    private CityRepo cityRepo;

    @Autowired
    private UserRepo userRepo;

    public List<PropertyDTO> getPropertyByFilter (PropertyFilterDTO propertyFilterDTO) {
        List<Property> properties = new ArrayList<>();
        if (CollectionUtils.isEmpty(properties)) {
            throw new PropertyException("Properties are not published.");
        }
        List<PropertyDTO> propertiesDTOS = new ArrayList<>();
        return propertyDtoMapper(properties, propertiesDTOS);
    }

    public String saveUpdateProperty (PropertyDTO propertyDTO) {
        String message = "Saved successfully";
        Property property = new Property();
        if (propertyDTO.getId() > 0) {
            property.setId(propertyDTO.getId());
            message = "Updated successfully";
        }
        propertyModelMapper (propertyDTO, property);
        propertyRepo.save(property);

        return message;
    }

    public String deleteProperty (PropertyDTO propertyDTO) {
        Optional<Property> propertyOptional = propertyRepo.findById(propertyDTO.getId());
        if (Objects.isNull(propertyOptional)) {
            throw new PropertyException("Property not found");
        }
        Property property = propertyOptional.get();
        property.setAvailablity(Boolean.FALSE);
        propertyRepo.save(property);

        return "Deleted Successfully";
    }

    private Property propertyModelMapper (PropertyDTO propertyDTO, Property property) {
        property.setName(propertyDTO.getTitle());
        property.setDescription(propertyDTO.getDescription());
        property.setAddress(propertyDTO.getAddress());
        property.setAdvertisementPublishDate(DateUtil.getFormatedDate(propertyDTO.getAdvertisementDate()));
        property.setAdvertisementExpireDate(DateUtil.getFormatedDate(propertyDTO.getAdvertisementExpireDate()));
        property.setRentFrequency(rentFrequencyRepo.getByName(propertyDTO.getRentFrequency()));
        property.setRent(propertyDTO.getRent());
        property.setPropertyType(propertyTypeRepo.getByName(propertyDTO.getPropertyType()));
        property.setAvailablity(propertyDTO.getAvailability());

        Country country = getCountryByName(propertyDTO.getCountry());
        City city = getCityNyName(propertyDTO.getCity());

        property.setCityCode(city.getCode());
        property.setCityId(city.getId());
        property.setCountruCode(country.getCode());
        property.setCountryId(country.getId());

        User user = userRepo.getOne(propertyDTO.getUserId());
        property.setUser(user);

        return property;
    }

    private Country getCountryByName (String name) {
        return countryRepo.getByName(name);
    }

    private City getCityNyName (String name) {
        return cityRepo.getByName(name);
    }

    private List<PropertyDTO> propertyDtoMapper (List<Property> properties, List<PropertyDTO> propertiesDTOS) {
        for (Property property : properties) {
            PropertyDTO propertyDTO = new PropertyDTO();
            propertyDTO.setId(property.getId());
            propertyDTO.setTitle(property.getName());
            propertyDTO.setDescription(property.getDescription());
            propertyDTO.setAddress(property.getAddress());
            propertyDTO.setZipCode(property.getZip_code());
            propertyDTO.setAdvertisementDate(DateUtil.convertCalendarToString(property.getAdvertisementExpireDate()));
            propertyDTO.setAdvertisementExpireDate(DateUtil.convertCalendarToString(property.getAdvertisementExpireDate()));

            Optional<Country> country = countryRepo.findById(property.getCountryId());
            Optional<City> city = cityRepo.findById(property.getCityId());

            propertyDTO.setCity(city.get().getName());
            propertyDTO.setCountry(country.get().getName());

            propertiesDTOS.add(propertyDTO);
        }
        return propertiesDTOS;
    }
}

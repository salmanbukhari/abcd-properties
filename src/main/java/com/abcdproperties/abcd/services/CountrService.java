package com.abcdproperties.abcd.services;


import com.abcdproperties.abcd.dto.DefaultDTO;
import com.abcdproperties.abcd.exception.PropertyException;
import com.abcdproperties.abcd.model.Country;
import com.abcdproperties.abcd.repository.CountryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CountrService {

    @Autowired
    private CountryRepo countryRepo;

    public List<DefaultDTO> getCountriesList () {
        List<Country> countries = countryRepo.findAll();
        if (CollectionUtils.isEmpty(countries)) {
            throw new PropertyException("Countries not found");
        }
        List<DefaultDTO> defaultDTOS = new ArrayList<>();
        return defaultDtoMapper(countries, defaultDTOS);
    }

    private List<DefaultDTO> defaultDtoMapper (List<Country> countries, List<DefaultDTO> defaultDTOS) {

        for (Country country : countries) {
            DefaultDTO defaultDTO = new DefaultDTO();
            defaultDTO.setId(country.getId());
            defaultDTO.setName(country.getName());
            defaultDTOS.add(defaultDTO);
        }
        return defaultDTOS;
    }
}

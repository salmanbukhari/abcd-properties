package com.abcdproperties.abcd.services;

import com.abcdproperties.abcd.dto.UserDTO;
import com.abcdproperties.abcd.exception.DataValidationException;
import com.abcdproperties.abcd.model.User;
import com.abcdproperties.abcd.repository.UserRepo;
import com.abcdproperties.abcd.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class UserDetailServiceImpl implements UserDetailService{

    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("Username " + email + " notfound");
        }
        return new UserDetailsImpl(user);
    }

    @Override
    public boolean checkEmailExist(final String email) {
        User user = userRepo.findByEmail(email);
        return !Objects.isNull(user);
    }

    @Override
    public String saveUser(UserDTO userRequest){
        User user;
        if(userRequest.getId() != null){
            user = userRepo.getOne(userRequest.getId());
            user = setUserData(user, userRequest);
        }else{
            if(checkEmailExist(userRequest.getEmail())){
                throw new DataValidationException("User against email already exists");
            }
            user = new User();
            user = setUserData(user, userRequest);
        }
        userRepo.save(user);
        return "User has been saved/updated successfully";
    }

    private User setUserData(User user, UserDTO userRequest){
        user.setFirstName(userRequest.getFirstName());
        user.setLastName(userRequest.getLastName());
        user.setEmail(userRequest.getEmail());
        user.setPassword(Util.bCryptSequence(userRequest.getPassword()));
        user.setContactNumber(userRequest.getContactNumber());
        return user;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null;
    }

    private final static class UserDetailsImpl implements UserDetails {

        private static final long serialVersionUID = 1L;

        private User user;

        private UserDetailsImpl(User user) {
            this.user = user;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            List<GrantedAuthority> authorities = new ArrayList<>(0);
            return authorities;
        }

        @Override
        public String getUsername() {
            return user.getEmail();
        }


        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

        @Override
        public String getPassword() {
            return user.getPassword();
        }

    }
}

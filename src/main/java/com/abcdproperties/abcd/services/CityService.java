package com.abcdproperties.abcd.services;


import com.abcdproperties.abcd.dto.DefaultDTO;
import com.abcdproperties.abcd.exception.PropertyException;
import com.abcdproperties.abcd.model.City;
import com.abcdproperties.abcd.repository.CityRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CityService {

    @Autowired
    private CityRepo cityRepo;

    public List<DefaultDTO> getCitiesList () {
        List<City> cities = cityRepo.findAll();
        if (CollectionUtils.isEmpty(cities)) {
            throw new PropertyException("Cities not found");
        }
        List<DefaultDTO> defaultDTOS = new ArrayList<>();
        return defaultDtoMapper(cities, defaultDTOS);
    }

    private List<DefaultDTO> defaultDtoMapper (List<City> cities, List<DefaultDTO> defaultDTOS) {

        for (City city : cities) {
            DefaultDTO defaultDTO = new DefaultDTO();
            defaultDTO.setId(city.getId());
            defaultDTO.setName(city.getName());
            defaultDTOS.add(defaultDTO);
        }
        return defaultDTOS;
    }
}

package com.abcdproperties.abcd.services;


import com.abcdproperties.abcd.dto.DefaultDTO;
import com.abcdproperties.abcd.exception.PropertyException;
import com.abcdproperties.abcd.model.Property;
import com.abcdproperties.abcd.model.PropertyType;
import com.abcdproperties.abcd.repository.PropertyTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PropertyTypeService {

    @Autowired
    private PropertyTypeRepo propertyTypeRepo;

    public List<DefaultDTO> getPropertyTypes () {
        List<PropertyType> propertyTypes = propertyTypeRepo.findAll();
        if (CollectionUtils.isEmpty(propertyTypes)) {
            throw new PropertyException("Property types not found");
        }
        List<DefaultDTO> defaultDTOS = new ArrayList<>();
        return defaultDtoMapper(propertyTypes, defaultDTOS);
    }

    private List<DefaultDTO> defaultDtoMapper (List<PropertyType> propertyTypes, List<DefaultDTO> defaultDTOS) {

        for (PropertyType propertyType : propertyTypes) {
            DefaultDTO defaultDTO = new DefaultDTO();
            defaultDTO.setId(propertyType.getId());
            defaultDTO.setName(propertyType.getName());
            defaultDTOS.add(defaultDTO);
        }
        return defaultDTOS;
    }
}

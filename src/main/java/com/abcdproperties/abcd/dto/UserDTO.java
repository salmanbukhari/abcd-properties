package com.abcdproperties.abcd.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class UserDTO implements Serializable {

    private static final long serialVersionUID = -1270016736970551024L;

    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private String password;
    private String contactNumber;

}

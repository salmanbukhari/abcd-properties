package com.abcdproperties.abcd.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PropertyDTO {

    private int id;
    private int userId;
    private String postedBy;
    private String rentFrequency;
    private String title;
    private String description;
    private String address;
    private String city;
    private String zipCode;
    private String country;
    private String rent;
    private String advertisementDate;
    private String advertisementExpireDate;
    private String propertyType;
    private Boolean availability;
}

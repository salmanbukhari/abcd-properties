package com.abcdproperties.abcd.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PropertyFilterDTO {

    private int id;
    private int offset;
    private int limit;
    private String name;
    private String rentFrequency;
    private String city;
    private String country;


}

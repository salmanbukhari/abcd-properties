package com.abcdproperties.abcd.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DefaultDTO {

    private int id;
    private String name;
    private String code;
}

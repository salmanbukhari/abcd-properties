package com.abcdproperties.abcd.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ResponseLogin implements Serializable {

    private static final long serialVersionUID = 3504768771086960317L;

    private String token;

}

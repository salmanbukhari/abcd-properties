package com.abcdproperties.abcd.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
public class RequestLogin implements Serializable {

    private static final long serialVersionUID = 5019682450643161640L;

    @NotNull
    private String email;
    @NotNull
    private String password;
}

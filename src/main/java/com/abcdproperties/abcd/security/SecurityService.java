package com.abcdproperties.abcd.security;

public interface SecurityService {

  Boolean hasProtectedAccess();

}

package com.abcdproperties.abcd.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "t_property")
public class Property implements Serializable {

    private static final long serialVersionUID = 8832728984270474769L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private int id;

    @ManyToOne
    @JoinColumn(name = "t_user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "t_rent_frequency_id")
    private RentFrequency rentFrequency;

    @ManyToOne
    @JoinColumn(name = "t_property_type_id")
    private PropertyType propertyType;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "zip_code", nullable = false)
    private String zip_code;

    @Column(name = "availablity", nullable = false)
    private boolean availablity;

    @Column(name = "advertisement_publish_date", nullable = false)
    private Calendar AdvertisementPublishDate;

    @Column(name = "advertisement_expire_date", nullable = false)
    private Calendar AdvertisementExpireDate;

    @Column(name = "rent", nullable = false)
    private String rent;

    @Column(name = "country_id", nullable = false)
    private int countryId;

    @Column(name = "country_code", nullable = false)
    private String countruCode;

    @Column(name = "city_id", nullable = false)
    private int cityId;

    @Column(name = "city_code", nullable = false)
    private String cityCode;


    public Property (){}

    public Property (int propertyId) {
        this.id = propertyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Property property = (Property) o;
        return id == property.id &&
                availablity == property.availablity &&
                countryId == property.countryId &&
                cityId == property.cityId &&
                cityCode == property.cityCode &&
                Objects.equals(user, property.user) &&
                Objects.equals(rentFrequency, property.rentFrequency) &&
                Objects.equals(propertyType, property.propertyType) &&
                Objects.equals(name, property.name) &&
                Objects.equals(description, property.description) &&
                Objects.equals(address, property.address) &&
                Objects.equals(zip_code, property.zip_code) &&
                Objects.equals(AdvertisementPublishDate, property.AdvertisementPublishDate) &&
                Objects.equals(AdvertisementExpireDate, property.AdvertisementExpireDate) &&
                Objects.equals(rent, property.rent) &&
                Objects.equals(countruCode, property.countruCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, rentFrequency, propertyType, name, description, address, zip_code, availablity, AdvertisementPublishDate, AdvertisementExpireDate, rent, countryId, countruCode, cityId, cityCode);
    }
}

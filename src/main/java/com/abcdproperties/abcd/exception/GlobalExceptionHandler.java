package com.abcdproperties.abcd.exception;

import com.abcdproperties.abcd.dto.ResponseError;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Optional;

@Log4j2
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseError> exceptionHandler(Exception exception) {
        log.error("Exception >>>> ", exception);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(getResponseError(HttpStatus.INTERNAL_SERVER_ERROR, getExceptionMessage(exception)));
    }

    @ExceptionHandler(DataValidationException.class)
    public ResponseEntity<ResponseError> dataValidationException(final DataValidationException dataValidationException){
        return error(dataValidationException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ResponseError> adCredentialsException(final BadCredentialsException badCredentialsException){
        return error(badCredentialsException, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<ResponseError> error(final Exception exception, final HttpStatus httpStatus) {
        log.error(exception);
        return ResponseEntity.status(httpStatus).body(getResponseError(httpStatus, getExceptionMessage(exception)));
    }

    private String getExceptionMessage(Exception exception){
        Throwable cause = exception.getCause()!=null?exception.getCause():exception;
        while(cause != null && cause.getCause() != null){
            cause = cause.getCause();
        }
        return Optional.of(cause.getMessage()).orElse(cause.getClass().getSimpleName());
    }

    private ResponseError getResponseError(final HttpStatus httpStatus, final String message){
        ResponseError error = new ResponseError();
        error.setErrorCode(httpStatus.value());
        error.setErrorMessage(message);
        return error;
    }

}

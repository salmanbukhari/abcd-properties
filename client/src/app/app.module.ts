import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import {GridviewComponent} from './gridview/gridview.component';
import {FooterComponent} from './footer/footer.component';
import { ProductListingComponent } from './product-listing/product-listing.component';
import { AddNewPropertyComponent } from './add-new-property/add-new-property.component';
import { SignupService} from "./signup/signup.service";

const appRoutes: Routes = [
  { path: 'signup', component: SignupComponent },
  { path: 'login',      component: LoginComponent },
  { path: '',      component: GridviewComponent },
  { path: 'add-new-property',      component: AddNewPropertyComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SignupComponent,
    LoginComponent,
    GridviewComponent,
    FooterComponent,
    ProductListingComponent,
    AddNewPropertyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [SignupService],
  bootstrap: [AppComponent]
})
export class AppModule { }

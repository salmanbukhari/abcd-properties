import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IResponseObject } from "../ResponseObject";
import { Observable } from "rxjs";
import { IUserObject } from "./userObject";




@Injectable()
export class SignupService {
  constructor (private http: HttpClient ) {}

  private _base_url: String = 'localhost:8080/';
  userData : IUserObject;

  checkEmailExists ( _email ) : Observable <IResponseObject> {
    return this.http.get<IResponseObject>(this._base_url + "checkEmailExists?email=" + _email);
  }

  signUp (userData) : Observable<IResponseObject> {
    return this.http.post<IResponseObject>(this._base_url + "signup" , userData);
  }

  login (reqLogin) : Observable<IResponseObject> {
    return this.http.post<IResponseObject>(this._base_url + "auth" , reqLogin);
  }
}

export interface IUserObject {
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  contactNumber: String

}

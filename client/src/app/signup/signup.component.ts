import { Component, OnInit } from '@angular/core';
import { SignupService } from "./signup.service";
import { IResponseObject } from "../ResponseObject";
import { IUserObject } from "./userObject";
import {IReqLogin} from "./reqLogin";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public response: IResponseObject;

  userObject : IUserObject;
  reqLogin : IReqLogin;

  constructor( private _signupService: SignupService ) { }

  checkEmailExists = ( _email ) => {
    this._signupService.checkEmailExists( _email ).subscribe( data => this.response = data )
  }

  signup = () => {
    this._signupService.signUp(this.userObject).subscribe(data => this.response = data);
  }

  login = () => {
    this._signupService.login(this.reqLogin).subscribe(data => this.response = data);
  }

  ngOnInit() {
  }

}
